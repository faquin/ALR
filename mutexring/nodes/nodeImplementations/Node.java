package projects.mutexring.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import projects.mutexring.nodes.messages.TokenMessage;
import projects.mutexring.nodes.messages.LcrMessage;
import projects.mutexring.nodes.messages.LeaderMessage;
import projects.mutexring.nodes.timers.MessageTimer;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;

public class Node extends sinalgo.nodes.Node {

    static final int MAX_UIN = 10000;
    private int myUin, uin;
    private boolean isRec, isLead;
    private static int currUin;

    @Override
    public void handleMessages(Inbox inbox) {
        while(inbox.hasNext()) {
            Message msg = inbox.next();
            if (msg instanceof LcrMessage) {
                treat((LcrMessage) msg);
            }
            if (msg instanceof LeaderMessage) {
                treatLead((LeaderMessage) msg);
            }
            if (msg instanceof TokenMessage) {
                deliver((TokenMessage) msg);
            }
        }
    }

    private void treat(LcrMessage m) {
        uin = m.getUin();
        System.out.println(this + " receives uin " + uin);
        isRec = true;
    }

    private void deliver(TokenMessage token) {
        System.err.println("token " + myUin + " " + isRec + " " + uin);
        if(!isRec)
            return;
        if (uin == myUin && !isLead) {
            this.setColor(Color.BLUE);
            System.out.println(this + " is the leader");
            isLead = true;
            broadcast(new LeaderMessage(uin));
            broadcast(token);
        } else if (uin > myUin) {
            System.out.println(this + " forwards " + uin);
            broadcast(new LcrMessage(uin));
        }
        if(!isLead)
            broadcast(token);
    }

    private void treatLead(LeaderMessage m) {
        int uin = m.getUin();
        System.out.println(this + " receives uin " + uin);
        if (uin == myUin) {
            this.setColor(Color.BLUE);
            System.out.println(this + " : I am the leader");
            isLead = true;
        } else if (uin > myUin) {
            this.setColor(Color.GREEN);
            System.out.println(this + " my leader is " + uin);
            broadcast(new LeaderMessage(uin));
        }
    }

    @Override
    public void init() {
        Random rand = sinalgo.tools.Tools.getRandomNumberGenerator();
        //~ myUin = rand.nextInt(MAX_UIN);
        //myUin = currUin++; // best case
        myUin = MAX_UIN - currUin++;
        isRec = false;
        isLead = false;
        System.out.println(this + " is initialized. UIN = " + myUin);
        new MessageTimer(new LcrMessage(myUin)).startRelative(1, this);
    }

    @Override
    public void draw(Graphics g, PositionTransformation pt, boolean highlight) {
        drawNodeAsDiskWithText(g, pt, highlight, (new Integer(ID).toString()), 25, Color.white);
    }

    @NodePopupMethod(menuText="broadcast")
    public void broadcast() {
        broadcast(new LcrMessage(myUin));
        broadcast(new TokenMessage());
    }

    @Override
    public void preStep() { }

    @Override
    public void neighborhoodChange() { }

    @Override
    public void postStep() {  }

    @Override
    public void checkRequirements() throws WrongConfigurationException { }

}
