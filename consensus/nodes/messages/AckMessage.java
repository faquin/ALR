package projects.consensus.nodes.messages;

import sinalgo.nodes.messages.Message;
import projects.consensus.nodes.nodeImplementations.Node;

public class AckMessage extends Message {

    final private Node    sender;
    final private Node   initiator;
    final private String msg;

    /**
     * @param i         : l'id du noeud emetteur de l'acquittement
     * @param msg       : une chaine de caractère représentant le message à acquitter
     * @param intiator  : l'arbre de recouvrement sur lequel l'acquittement
     *                    voyage
     */
    public AckMessage(Node i, String msg, Node initiator) {
        this.sender    = i;
        this.initiator = initiator;
        this.msg       = msg;
    }

    public String getMsg() {
        return msg;
    }

    public Node getInitiator() {
        return initiator;
    }

    public Node getSender() {
        return sender;
    }

    @Override
    public Message clone() {
        return new AckMessage(sender, msg, initiator);
    }

}
