package projects.consensus.nodes.messages;

import sinalgo.nodes.messages.Message;
import projects.consensus.nodes.nodeImplementations.Node;

public class SpanningMessage extends Message {

    final private Node    sender;
    final private Node   initiator;
    final private String msg;

    public SpanningMessage(Node i, String m, Node initiator) {
        this.sender    = i;
        this.msg       = m;
        this.initiator = initiator;
    }

    public Node getInitiator() {
        return initiator;
    }

    public Node getSender() {
        return sender;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public Message clone() {
        return new SpanningMessage(sender, msg, initiator);
    }

}
