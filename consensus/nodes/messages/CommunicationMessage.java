package projects.consensus.nodes.messages;

import sinalgo.nodes.messages.Message;
import projects.consensus.nodes.nodeImplementations.Node;

public class CommunicationMessage extends Message {

    final private Node     destination;
    final private Node     initiator;
    final private String[] msg;

    public CommunicationMessage(Node init, Node destination, String ... messages) {
        this.destination = destination;
        this.initiator   = init;
        this.msg         = messages;
    }

    public String[] getMsg() {
        return msg;
    }

    public Node getInitiator() {
        return initiator;
    }

    public Node getDestination() {
        return destination;
    }

    @Override
    public Message clone(){
        return new CommunicationMessage(initiator, destination,msg);
    }
}
