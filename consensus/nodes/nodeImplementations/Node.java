package projects.consensus.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.*;

import projects.consensus.nodes.messages.SpanningMessage;
import projects.consensus.nodes.messages.CommunicationMessage;
import projects.consensus.nodes.messages.AckMessage;
import projects.consensus.nodes.timers.MessageTimer;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;
import sinalgo.nodes.edges.Edge;
import sinalgo.runtime.AbstractCustomGlobal;
import sinalgo.tools.Tools;

public class Node extends sinalgo.nodes.Node {

    static final int     MAX_UIN = 10000;
    static       boolean blue    = true;
    static       int     currUin = 0;
    static       int     nbMsg   = 0;

    private HashMap<Node, Node>                     fathers   = null;
    private HashMap<Node, ArrayList<Node>>          sons      = null;
    private HashMap<Node, HashMap<String, Integer>> ackWait   = null;
    private int                                     myUin     = 0;
    private String                                  myColor   = "";
    private int                                     waitColor = 0;
    private ArrayList<String>                       colors    = null;

    public Node(){
        fathers = new HashMap<Node, Node>();
        sons    = new HashMap<Node, ArrayList<Node>>();
        ackWait = new HashMap<Node, HashMap<String, Integer>>();
    }

    @Override
    public void handleMessages(Inbox inbox) {
        try{
            while(inbox.hasNext()) {
                Message msg = inbox.next();
                if (msg instanceof SpanningMessage) {
                    System.out.println(myUin+" SpanningMessage reçu");
                    treat((SpanningMessage) msg);
                } else if (msg instanceof CommunicationMessage){
                    System.out.println(myUin+" Communication msg reçu");
                    treat((CommunicationMessage) msg);
                } else {
                    System.out.println(myUin+" ACK reçu");
                    treat((AckMessage) msg);
                }
            }
        }catch(Throwable t){
            t.printStackTrace();
        }
    }

    /*
     * Traite les messages de communication
     */
    private void treat(CommunicationMessage msg){
        Node initiator   = msg.getInitiator();
        Node destination = msg.getDestination();
        //Si je suis le destinataire du message, alors, je renvoie la couleur
        //sinon je fais passer le message à mon père sur la branche de la
        //destination.
        if(destination.getUin() == this.getUin()){
            //Si on me demande quel est ma couleur, alors renvoyer à
            //l'initiateur ma couleur.
            if(msg.getMsg()[0] == "what color"){
                send(new CommunicationMessage(this, initiator,
                            "color is", myColor), fathers.get(initiator));
            }
            //Si je reçois la couleur d'un membre
            if(msg.getMsg()[0] == "color is"){
                //diminuer le compteur d'attentes
                waitColor--;
                //Si le compteur d'attente atteins zéro, alors
                if(waitColor == 0){
                    int blue  = 0;
                    int green = 0;
                    //parcourir les couleurs reçues et choisir la couleur
                    //qui a vaincu le duel
                    for(String color : colors){
                        if(color == "blue"){
                            blue++;
                        }else{
                            green++;
                        }
                    }
                    //Demande à tous les noeuds du graphe de mettre à jour
                    //leur couleur
                    changeColor(blue > green ? "blue" : "green");
                    if(blue > green)
                        this.setColor(Color.BLUE);
                    else
                        this.setColor(Color.GREEN);
                }else{
                    colors.add(msg.getMsg()[1]);
                }
            }
            //Si on me demande de changer de couleur, alors, j'effectue le
            //changement.
            if(msg.getMsg()[0] == "change color"){
                if(msg.getMsg()[1] == "blue"){
                    this.setColor(Color.BLUE);
                }else{
                    this.setColor(Color.GREEN);
                }
            }
        }else{
            //faire passer le message à la destination
            Node pere = fathers.get(destination);
            send((CommunicationMessage)msg.clone(), pere);
        }
    }

    /*
     * Gestion de l'acquittement.
     *
     * Un acquittement est associé à un message particulier sur un arbre donné.
     */
    private void treat(AckMessage msg){
        Node  initiator = msg.getInitiator();
        if(ackWait.get(initiator) != null){
            int   nbFils    = ackWait.get(initiator).get(msg.getMsg());
            //A la réception d'un acquittement, il faut vérifier que le nombre
            //d'acquittement attendus est égal à 1. Dans ce cas envoyer un
            //acquittement au père sur l'abre de l'initiateur. Dans le cas
            //contraire, décrémenter le nombre de fils d'un can.
            if(nbFils > 1){
                nbFils--;
            }else {
                send(new AckMessage(this, msg.getMsg(), initiator), fathers.get(initiator));
            }
        }
    }

    /*
     * Gestion du message de construction de l'arbre de recouvrement
     *
     * Chaque SpanningMessage est associé à un initiateur.
     */
    private void treat(SpanningMessage m) {
        Node initiator = m.getInitiator();
        //Dans le cas où pour un arbre en construction le noeud courant n'a pas
        //de père, déclarer que sur cet arbre, le père du noeud courant est le
        //sender.
        if (fathers.get(initiator) == null){
            fathers.put(initiator, m.getSender());
            //Puis, une fois cette déclaration effectuée, propager la
            //construction de l'abre à tous les fils du noeud courant.
            //Les fils sont l'ensemble des connexions excepté le père.
            ArrayList<Node> fils = new ArrayList<Node>();
            int nbFils = 0;
            for(Edge e: outgoingConnections) {
                Node dest = (Node) e.endNode;
                if (!dest.equals(fathers.get(initiator))) {
                    nbFils++;
                    send(new SpanningMessage(this, m.getMsg(), initiator), dest);
                    fils.add(dest);
                }
            }
            //Si le noeud courant a au moins un fils, alors, mettre à jour pour
            //l'abre en cours de création la liste des fils connus et envoyer
            //mettre à jour le nombre d'acquittement à attendre avant de
            //propager l'acquittement au père.
            if(nbFils>0){
                sons.put(initiator, fils);
                HashMap<String, Integer> a = new HashMap<String, Integer>();
                a.put(m.getMsg(), nbFils);
                ackWait.put(initiator,a);
            }else{
                // Sinon, il se trouve que le noeud courant est sur une feuille
                // de l'arbre en cours de création. Du coup, avertir le père
                // avec un message d'acquittement.
                HashMap<String, Integer> a = new HashMap<String, Integer>();
                a.put(m.getMsg(), nbFils);
                ackWait.put(initiator,a);
                send(new AckMessage(this, m.getMsg(), initiator), m.getSender());
            }
        }
        else {
            //Dans le cas où le noeud courant a déjà un père de déclaré et qu'il
            //reçoit un message de construction d'arbre avec le même initiateur,
            //alors envoyer un message d'acquittement à l'émetteur.
            if(sons.get(m.getInitiator()).contains(m.getSender())){
                send(new AckMessage(this, m.getMsg(), initiator), m.getSender());
            }
        }
    }

    //public Node getFather() {}

    @Override
    public void init() {
        //~ Random rand = sinalgo.tools.Tools.getRandomNumberGenerator();
        currUin++;
        myUin = currUin;
        System.out.println(this + " is initialized. UIN = " + myUin);
        //lancement d'un arbre pour chaque noeud
        //TODO être soit vert soit bleue à l'init
        if(blue){
            this.setColor(Color.BLUE);
            myColor = "blue";
        }else{
            this.setColor(Color.GREEN);
            myColor = "green";
        }
        blue = !blue;
        broadcast(new SpanningMessage(this, "toto", this));
    }

    @Override
    public void draw(Graphics g, PositionTransformation pt, boolean highlight) {
        drawNodeAsDiskWithText(g, pt, highlight, (new Integer(ID).toString()), 25, Color.white);
    }


    @NodePopupMethod(menuText="broadcast")
    public void initButton(){
        for(sinalgo.nodes.Node n : Tools.getNodeList()) 
            n.init();
    }

    @NodePopupMethod(menuText="choose color")
    public void chooseColor(){
        try{
            whatColor();
        }catch(Throwable t){
            t.printStackTrace();
        }
    }

    /*
     * Demande à chaque noeud du graphe de changer sa couleur par celle passée
     * en paramètre
     */
    private void changeColor(String color){
        for(Iterator<Map.Entry<Node, Node>> i = fathers.entrySet().iterator(); i.hasNext();){
            Map.Entry<Node, Node> row = i.next();
            Node destination = row.getKey();
            Node pere        = row.getValue();
            send(new CommunicationMessage(this,destination, "change color", color),pere);
        }
    }

    /*
     * Demande à chaque noeud du graphe quel est sa couleur
     */
    private void whatColor(){
        for(Iterator<Map.Entry<Node, Node>> i = fathers.entrySet().iterator(); i.hasNext();){
            Map.Entry<Node, Node> row = i.next();
            Node destination = row.getKey();
            Node pere        = row.getValue();
            send(new CommunicationMessage(this,destination, "what color"),pere);
            //incrémente le nombre de messages de retour à attendre avant de
            //déclarer une couleur vainqueur
            waitColor++;
        }
        //initialiser une liste pour recevoir les couleurs des noeuds du graphe
        colors = new ArrayList<String>();
    }


    public int getUin(){
        return myUin;
    }

    @Override
    public void preStep() { }

    @Override
    public void neighborhoodChange() { }

    @Override
    public void postStep() {  }

    @Override
    public void checkRequirements() throws WrongConfigurationException { }

}
