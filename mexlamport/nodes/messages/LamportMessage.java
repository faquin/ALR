package projects.mexlamport.nodes.messages;

import sinalgo.nodes.messages.Message;

public abstract class LamportMessage extends Message {
	
	int senderId;
	int clock;
		
	LamportMessage(int s, int c) {
		senderId = s;
		clock = c;
	}
	
	public int getSenderId() { return senderId;}
	
	public int getClock() { return clock; }
	
	@Override
	public Message clone() {
		return this;
	}

}
