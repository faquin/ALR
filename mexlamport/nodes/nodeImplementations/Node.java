package projects.mexlamport.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TreeMap;
import java.util.Random;
import java.util.Set;

import projects.mexlamport.nodes.messages.ExitMessage;
import projects.mexlamport.nodes.messages.LamportMessage;
import projects.mexlamport.nodes.messages.ReplyMessage;
import projects.mexlamport.nodes.messages.RequestMessage;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.edges.Edge;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;

public class Node extends sinalgo.nodes.Node {
	
	
	Queue<Request> queue = new PriorityQueue<Request>();
    
    private int monId;
    private static int currId;
    static final int MAX_UIN = 10000;
    
    private int monHorloge = 0;
    
    private int replyReceived = 0;
    
    private TreeMap<Integer,Integer> waitlist;
	
	@Override
	public void handleMessages(Inbox inbox) {
		while(inbox.hasNext()) {
			LamportMessage msg = (LamportMessage) inbox.next();
            
            monHorloge = msg.getClock() < monHorloge ? monHorloge+1 : msg.getClock()+1;
            
		    if (msg instanceof RequestMessage){
                treat((RequestMessage) msg);
            }
            else if (msg instanceof ReplyMessage){
                treat((ReplyMessage) msg);
            }
            else if (msg instanceof ExitMessage){
                treat((ExitMessage) msg);
            }
		}
	}
		
	

	@Override
	public void init() {
		System.out.println(this + " is initialized.");
        Random rand = sinalgo.tools.Tools.getRandomNumberGenerator();
		monId = rand.nextInt(MAX_UIN);
		monId = currId++; // best case
        
        monHorloge = rand.nextInt(10);
        
        waitlist = new TreeMap<Integer,Integer>();
        
        broadcast(new RequestMessage(monId, monHorloge));
        
        replyReceived = 0;
	}
    
    private void treat(RequestMessage msg){
        
        waitlist.put(msg.getClock(),msg.getSenderId());
        
        send((new ReplyMessage(monId, monHorloge)), msg.getSenderId());
        monHorloge++;
    }
    
    private void treat(ReplyMessage msg){
		replyReceived++;
		
		if (replyReceived == outgoingConnections.size()){
			enterCriticSection();
		}
	}
	
	private void treat(ExitMessage msg){
		Set<Integer> keys = waitlist.keySet(); 
		waitlist.remove(first(keys));
		
		enterCriticSection();
		
	}
    
    private Integer first(Set<Integer> set){
		Object[] array = set.toArray();
		
		if(array.length > 0) {
			return (Integer) array[0];
		} else {
			return null;
		}
		
	}
    
    private void enterCriticSection(){
		Set<Integer> keys = waitlist.keySet(); 
			
		if(first(keys) != null && first(keys) == monId){
			System.out.println("Section critique : "+monId);
			
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			waitlist.remove(first(keys));
			
			broadcast(new ExitMessage(monId,monHorloge));
			monHorloge++;
		}
	}
    
    //~ private void broadcast();

	@Override
	public void draw(Graphics g, PositionTransformation pt, boolean highlight) {
		drawNodeAsDiskWithText(g, pt, highlight, (new Integer(ID).toString()), 25, Color.white);
	}

	@NodePopupMethod(menuText="try")
	public void mexTry() {
		
	}
	
	@NodePopupMethod(menuText="exit")
	public void mexExit() {
				
	}
	

	// DO NOT MODIFY BELOW
	
	@Override
	public void preStep() { }

	@Override
	public void neighborhoodChange() { }

	@Override
	public void postStep() {  }

	@Override
	public void checkRequirements() throws WrongConfigurationException { }
	
	public void send(Message m, int dest) {
		System.out.println(this + " sent message " + m + " to " + dest);
		boolean sent = false;
		for (Edge e : outgoingConnections) 
			if (e.endNode.ID == dest) {
				sent = true;
				send(m, e.endNode);
				break;
		}
		if (!sent) {
			throw new RuntimeException("No direct link between nodes " + this.ID + " and " + dest);
		}
	}
	
}
