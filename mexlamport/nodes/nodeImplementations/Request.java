package projects.mexlamport.nodes.nodeImplementations;

public class Request implements Comparable<Request> {
	
	final int time;
	final int procId;
	
	public Request(int time, int procId) {
		this.time = time;
		this.procId = procId;
	}
	
	public int getTime() {
		return time;
	}
	
	public int getProcId() {
		return procId;
	}
	
	public int compareTo(Request lt) {
		if ((this.time < lt.time) || (this.time == lt.time && this.procId < lt.procId)) return -1;
		if (this.time == lt.time && this.procId == lt.procId) return 0;
		return 1;
	}
	
}
