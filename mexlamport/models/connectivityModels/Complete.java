package projects.mexlamport.models.connectivityModels;

import java.util.Enumeration;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.nodes.Node;
import sinalgo.runtime.nodeCollection.NodeCollectionInterface;
import sinalgo.tools.Tools;

public class Complete extends sinalgo.models.ConnectivityModel  {
	
	public boolean updateConnections(Node n) throws WrongConfigurationException{
		boolean edgeAdded = false;
		
		NodeCollectionInterface pNE = Tools.getNodeList(); 
		Enumeration<Node> enu = pNE.getNodeEnumeration();
		while( enu.hasMoreElements() ){
			Node possibleNeighbor = enu.nextElement();
			if(n.ID != possibleNeighbor.ID){
				// if the possible neighbor is connected with the the node: add the connection to the outgoing connection of n 
					// add it to the outgoing Edges of n. The EdgeCollection itself checks, if the Edge is already contained
					edgeAdded = !n.outgoingConnections.add(n, possibleNeighbor, true) || edgeAdded; // note: don't write it the other way round, otherwise, the edge is not added if edgeAdded is true.
			}
		}
		// loop over all edges again and remove edges that have not been marked 'valid' in this round
		boolean dyingLinks = n.outgoingConnections.removeInvalidLinks(); 

		return edgeAdded || dyingLinks; // return whether an edge has been added or removed.
	}	
}
