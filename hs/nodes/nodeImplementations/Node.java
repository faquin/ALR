package projects.hs.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import projects.defaultProject.nodes.timers.MessageTimer;
import projects.hs.nodes.messages.HsMessage;
import projects.hs.nodes.messages.HsMessage.Dir;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.edges.Edge;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;

public class Node extends sinalgo.nodes.Node {
	
	static final int MAX_UIN = 10000;
	private int myUin;
	private int received;
	private int l;
	private static int nSend = 0;
	private boolean isLeader;
	
	@Override
	public void handleMessages(Inbox inbox) {
		while(inbox.hasNext()) {
			Message msg = inbox.next();
			if (msg instanceof HsMessage) {
				handle((HsMessage) msg);
			} 
		}
	}
		
	private void handle(HsMessage m) {
		if(m.getDir()==Dir.OUT){
			if(m.getUin()>myUin){
				if(m.getHop() != 0){
					for(Edge e : this.outgoingConnections){
						Node n = (Node) e.endNode;
						if(n != m.getSender()){
							nSend++;
							this.send(new HsMessage(m.getUin(), this, m.getDir(), m.getHop()-1), n);
						}
					}
				}else{
					nSend++;
					this.send(new HsMessage(m.getUin(), this, Dir.IN, 0), m.getSender());
				}
			}else if(m.getUin() == myUin){
				isLeader = !isLeader;
				if(isLeader){
					this.setColor(Color.RED);
					System.out.println(myUin + " est le leader!");
					System.out.println("nbSend : " + nSend);
				}
			}
		}else{
			if(m.getUin() != myUin){
				for(Edge e : this.outgoingConnections){
					Node n = (Node) e.endNode;
					if(n != m.getSender()){
						nSend++;
						this.send(new HsMessage(m.getUin(), this, m.getDir(), m.getHop()-1), n);
					}
				}
			}else{
				received++;
				if(received == 2){
					received = 0;
					l++;
					nSend += 2; // gauche et droite
					isLeader = true;
					this.broadcast(new HsMessage(myUin, this, Dir.OUT, (int)Math.pow(2, l)));
				}
			}
		}
	}
		

	@Override
	public void init() {
	    Random rand = sinalgo.tools.Tools.getRandomNumberGenerator();
	    myUin = rand.nextInt(MAX_UIN); 
	    received = 0;
	    l = 0;
		System.out.println(this + " is initialized. UIN = " + myUin);
		new MessageTimer(new HsMessage(myUin, this, HsMessage.Dir.OUT, 1)).startRelative(1, this);
	}
	
	@Override
	public void draw(Graphics g, PositionTransformation pt, boolean highlight) {
		drawNodeAsDiskWithText(g, pt, highlight, (new Integer(ID).toString()), 25, Color.white);
	}

	@Override
	public void preStep() { }

	@Override
	public void neighborhoodChange() { }

	@Override
	public void postStep() {  }

	@Override
	public void checkRequirements() throws WrongConfigurationException { }
	
	
	
}
