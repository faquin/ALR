package projects.hs.nodes.messages;

import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

public class HsMessage extends Message {
	
	public static enum Dir { OUT, IN }//out sort in reviens

	final private int uin;//Id de l'envoyeur
	final private Node sender;//source
	final private Dir dir;//Direction dans lequel le message transite
	final private int hop;//Distances restante à parcouri
	
	public HsMessage(int i, Node sender, Dir dir, int hop) {
		uin          = i;
		this.sender  = sender;
		this.hop     = hop;
		this.dir     = dir;
	}
	
	public int getUin() {
		return uin;
	}
	
	public Node getSender() {
		return sender;
	}
	
	public Dir getDir() {
		return dir;
	}
	
	public int getHop() {
		return hop;
	}
	
	@Override
	public Message clone() {
		return this;
	}

}
