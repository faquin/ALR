package projects.peterson.nodes.messages;

import sinalgo.nodes.messages.Message;

public class PetersonMessage extends Message {

	final private int uin;
	private int distance;

	public PetersonMessage(int i, int distance) {
		this.uin      = i;
		this.distance = distance;
	}

	public void decrementerDistance() {
		distance--;
	}

	public int getDistance() {
		return distance;
	}

	public int getUin() {
		return uin;
	}

	@Override
	public Message clone() {
		return this;
	}

}
