package projects.peterson.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import projects.peterson.nodes.messages.PetersonMessage;
import projects.peterson.nodes.messages.LeaderMessage;
import projects.peterson.nodes.timers.MessageTimer;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;

public class Node extends sinalgo.nodes.Node {

	static final int MAX_UIN = 10000;
	private int      myUin;
	private int      prevU1 = -1;
	private int      prevU2 = -1;
	private boolean  actif  = true;

	@Override
	public void handleMessages(Inbox inbox) {
		while(inbox.hasNext()) {
			Message msg = inbox.next();
			if (msg instanceof PetersonMessage) {
				treat((PetersonMessage) msg);
			}
			if (msg instanceof LeaderMessage) {
				treatLead((LeaderMessage) msg);
			}
		}
	}

	private void treat(PetersonMessage m) {
		int uin = m.getUin();
		System.out.println(this + " receives uin " + uin);
		System.out.println(this + " is active ? " + actif);
		if(actif){
			//Si actif, faire le code intelligent
			int distance = m.getDistance();
			if(m.getUin() != myUin){
				//Dans le cas où la distance vaut 1, il s'agit de l'id de
				//l'avant dernière machine
				if(distance == 1){
					prevU2 = m.getUin();
				}
				//Dans le cas où la distance vaut 2, il s'agit de l'id de
				//la précédente machine
				else if(distance == 2){
					prevU1 = m.getUin();
					//Décrémenter la distance en transmettre
					m.decrementerDistance();
					broadcast(m);
				}
				if(prevU1 != -1 && prevU2 != -1){
					if(prevU1 > Math.max(myUin, prevU2)){
						myUin = prevU1;
					}else{
						actif = false;
					}
					prevU1 = prevU2 = -1;
					broadcast(new PetersonMessage(myUin, 2));
				}
			}else{
				System.out.println(this + " Je suis leader"+ myUin);
				broadcast(new LeaderMessage(myUin));
			}
		}else{
			//Sinon, faire passe plat
            if(m.getUin() != myUin){
                broadcast(m);
            }
		}
	}

	private void treatLead(LeaderMessage m) {
		int uin = m.getUin();
		System.out.println(this + " receives uin " + uin);
		if (uin == myUin) {
			this.setColor(Color.BLUE);
			System.out.println(this + " : I am the leader");
		} else if (uin > myUin) {
			this.setColor(Color.GREEN);
			System.out.println(this + " my leader is " + uin);
			broadcast(new LeaderMessage(uin));
		}	
	}

	@Override
	public void init() {
		Random rand = sinalgo.tools.Tools.getRandomNumberGenerator();
		myUin = rand.nextInt(MAX_UIN);
		System.out.println(this + " is initialized. UIN = " + myUin);
		new MessageTimer(new PetersonMessage(myUin, 2)).startRelative(1, this);
	}

	@Override
	public void draw(Graphics g, PositionTransformation pt, boolean highlight) {
		drawNodeAsDiskWithText(g, pt, highlight, (new Integer(ID).toString()), 25, Color.white);
	}

	@NodePopupMethod(menuText="broadcast")
	public void broadcast() {
		broadcast(new PetersonMessage(myUin, 2));
	}

	@Override
	public void preStep() { }

	@Override
	public void neighborhoodChange() { }

	@Override
	public void postStep() {  }

	@Override
	public void checkRequirements() throws WrongConfigurationException { }

}
