package projects.spanning.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import java.util.ArrayList;

import projects.spanning.nodes.messages.SpanningMessage;
import projects.spanning.nodes.messages.AckMessage;
import projects.spanning.nodes.timers.MessageTimer;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;
import sinalgo.nodes.edges.Edge;
import sinalgo.runtime.AbstractCustomGlobal;
import sinalgo.tools.Tools;

public class Node extends sinalgo.nodes.Node {

	static final int MAX_UIN = 10000;
    static int currUin = 0;
    static int       nbMsg = 0;
    private int      nbFils=0;
	private int      myUin;
	private int      prevU1 = -1;
	private int      prevU2 = -1;
	private boolean  actif  = true;
    
    private int pere = -1;

	@Override
	public void handleMessages(Inbox inbox) {
		while(inbox.hasNext()) {
			Message msg = inbox.next();
			if (msg instanceof SpanningMessage) {
				treat((SpanningMessage) msg);
			} else {
                treat((AckMessage) msg);
            }
        }
    }
    
    /*
     * Lors de la reception d'un AckMessage, le noeud va décrémenter son nombre de fils
     * et lorsque le compte atteint 1 il trabsmet un AckMessage à son père.
     */

    private void treat(AckMessage msg){
        System.out.println(myUin+" recu ACKmessage de :" + msg.getSender());
        if(nbFils > 1)
            nbFils--;
        else {
            for(Edge e: outgoingConnections) {
                Node voisin = (Node) e.endNode;

                if (voisin.getUin() == pere) {
                    send(new AckMessage(getUin()), voisin);
                    System.out.println(myUin+" j'acquitte");
                    setColor(Color.GREEN);
                }
            }
        }
    }
    /*
     * Quand un noeud recoit un spanning message, la première fois
     * il met à jour l'identité de son père et lors des receptions suivantes
     * il renverra un AckMessage à l'envoyeur.
     */
	private void treat(SpanningMessage m) {
        System.out.println(myUin+" recu message de :" + m.getSender());
        nbMsg++;
        System.out.println(myUin + " nb de message recus :"+nbMsg);
        if (pere == -1){
            pere = m.getSender();
            
            for(Edge e: outgoingConnections) {
                Node dest = (Node) e.endNode;
                
                if (dest.getUin() != pere) {
                    send(new SpanningMessage(getUin(), m.getMsg()), dest);
                    nbFils++;
                }
            }
        }
        else {
            for(Edge e: outgoingConnections) {
                Node voisin = (Node) e.endNode;

                if (voisin.getUin() == pere) {
                    send(new AckMessage(getUin()), voisin);
                    setColor(Color.GREEN);
                }
            }
        }
	}
    
    //public Node getFather() {}

	@Override
	public void init() {
		//~ Random rand = sinalgo.tools.Tools.getRandomNumberGenerator();
        currUin++;
		myUin = currUin;
		System.out.println(this + " is initialized. UIN = " + myUin);
	}

	@Override
	public void draw(Graphics g, PositionTransformation pt, boolean highlight) {
		drawNodeAsDiskWithText(g, pt, highlight, (new Integer(ID).toString()), 25, Color.white);
	}

	@NodePopupMethod(menuText="broadcast")
	public void broadcast() {
        setColor(Color.BLUE);
		broadcast(new SpanningMessage(myUin, "toto"));
	}
    
    @AbstractCustomGlobal.CustomButton(buttonText="Init")
    public void initButton(){
        for(sinalgo.nodes.Node n : Tools.getNodeList()) n.init();
    }

    public int getUin(){
        return myUin;
    }

	@Override
	public void preStep() { }

	@Override
	public void neighborhoodChange() { }

	@Override
	public void postStep() {  }

	@Override
	public void checkRequirements() throws WrongConfigurationException { }

}
