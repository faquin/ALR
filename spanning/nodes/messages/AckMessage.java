package projects.spanning.nodes.messages;

import sinalgo.nodes.messages.Message;

public class AckMessage extends Message {

	final private int sender;

	public AckMessage(int i) {
		this.sender      = i;
	}

	public int getSender() {
		return sender;
	}
    
	@Override
	public Message clone() {
		return new AckMessage(sender);
	}

}
