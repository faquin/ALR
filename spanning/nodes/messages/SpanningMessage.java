package projects.spanning.nodes.messages;

import sinalgo.nodes.messages.Message;

public class SpanningMessage extends Message {

	final private int sender;
    final private String msg; 

	public SpanningMessage(int i, String m) {
		this.sender      = i;
        this.msg = m;
	}

	public int getSender() {
		return sender;
	}
    
	public String getMsg() {
		return msg;
	}

	@Override
	public Message clone() {
		return new SpanningMessage(sender, msg);
	}

}
