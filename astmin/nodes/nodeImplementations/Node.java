package projects.astmin.nodes.nodeImplementations;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import java.util.ArrayList;

import projects.astmin.nodes.messages.AstMessage;
import projects.astmin.nodes.messages.AstUpdate;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.edges.Edge;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;
import sinalgo.runtime.AbstractCustomGlobal;
import sinalgo.tools.Tools;

public class Node extends sinalgo.nodes.Node {
    
    static final int MAX_UIN = Integer.MAX_VALUE;
    
    private Integer myUin;
    private Node pere;
    private Integer range;
    private AstUpdate upd;
    private Boolean sender;
    private static int nbM = 0;
    
    static int receive = 0;
    
    @Override
    public void handleMessages(Inbox inbox) {
        while(inbox.hasNext()) {
            Message msg = inbox.next();
            if (msg instanceof AstMessage) {
                System.out.println("ast message a traiter");
                treat((AstMessage) msg);
            }else if (msg instanceof AstUpdate) {
                System.out.println("ast update a traiter");
                treat((AstUpdate) msg);
            }
        }
    }
    
    private void treat(AstMessage m) {
        nbM++;
        System.out.println(this + " " + nbM
        + " messages reçut dans treat(AstMessage)!");
        if(pere == null || m.range < this.range){
            pere = m.pere;
            range = m.range;
            for(Edge e : this.outgoingConnections){
                Node n = (Node) e.endNode;
                if(n != pere){
                    this.send(new AstMessage(this, m.range+1), n);
                }
            }
            sendUpdate();
        }
    }

    private void treat(AstUpdate m) {
        nbM++;
        System.out.println(this + " " + nbM
        + " messages reçut dans treat(AstUpdate)!");
        if(!sender){
            this.send(m, pere);
        }else{
            upd.peres.addAll(m.peres);
            upd.nodes.addAll(m.nodes);
            upd.ranks.addAll(m.ranks);

            // print
            Integer size = upd.peres.size();
            
            ArrayList<Integer> peres = upd.peres;
            ArrayList<Integer> nodes = upd.nodes;
            ArrayList<Integer> ranks = upd.ranks;
            
            //Ici on doit nettoyer les chemins les plus long accèdant au même noeud. Pour obtenir l'arbre minimal.
            ArrayList<Integer> ptmp = new ArrayList<Integer>();
            ArrayList<Integer> ftmp = new ArrayList<Integer>();
            ArrayList<Integer> rtmp = new ArrayList<Integer>();
            
            Integer tmpSize = 0;
            
            for (int i = 0; i < size; i++) {
                if(!ftmp.contains(nodes.get(i))){
                    ptmp.add(peres.get(i));
                    ftmp.add(nodes.get(i));
                    rtmp.add(ranks.get(i));
                    for (int j = 0; j < size; j++) {
                        if(ftmp.get(tmpSize) == nodes.get(j)
                            && rtmp.get(tmpSize) > ranks.get(j)){
                            ptmp.set(tmpSize, peres.get(j));
                            rtmp.set(tmpSize, ranks.get(j));
                        }
                    }
                    tmpSize++;
                }
            }
            
            System.out.println("Graphe de recouvrement minimal :");
            for (int i = 0; i < tmpSize; i++) {
                for (int j = 0; j <= rtmp.get(i); j++) {
                    System.out.print("\t");
                }
                System.out.println("Sender : "
                    +ptmp.get(i)+"->"+ftmp.get(i)
                    +", distance : "+rtmp.get(i));
            }
            System.out.println("==============================");
        }
    }

    private void sendUpdate(){
        System.out.println("sendUpd!");
        upd.peres.add(pere.myUin);
        upd.nodes.add(myUin);
        upd.ranks.add(range);
        this.send(upd.clone(), pere);
    }


    @Override
    public void init() {
        setColor(Color.BLACK);
        Random rand = sinalgo.tools.Tools.getRandomNumberGenerator();
        myUin = rand.nextInt(MAX_UIN);
        
        pere = null;
        range = Integer.MAX_VALUE;
        
        sender = false;
        upd = new AstUpdate();
        
        System.out.println(this + " is initialized. UIN = " + myUin);
    }

    @Override
    public void draw(Graphics g, PositionTransformation pt, boolean highlight) {
        drawNodeAsDiskWithText(g, pt, highlight, (new Integer(ID).toString()), 25, Color.white);
    }
    
    @AbstractCustomGlobal.CustomButton(buttonText="Init", toolTipText="Reset defaut Color")
    public void initButton(){
        for(sinalgo.nodes.Node n : Tools.getNodeList()) n.init();
    }

    @NodePopupMethod(menuText="broadcast")
    public void broadcast() {
        this.setColor(Color.RED);
        pere = this;
        range = -1;
        sender = true;
        System.out.println(this + " broadcast a message");
        broadcast(new AstMessage(this, 0));
    }

    @Override
    public void preStep() { }

    @Override
    public void neighborhoodChange() { }

    @Override
    public void postStep() {  }

    @Override
    public void checkRequirements() 
        throws WrongConfigurationException { }
    
}
