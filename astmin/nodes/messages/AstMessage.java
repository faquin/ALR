package projects.astmin.nodes.messages;

import projects.astmin.nodes.nodeImplementations.Node;
import sinalgo.nodes.messages.Message;

public class AstMessage extends Message {
    
    public final Node pere;
    public final Integer range;
    
    public AstMessage(Node node, Integer range) {
        this.pere = node;
        this.range = range;
    }
    
    @Override
    public Message clone() {
        return this;
    }

}
