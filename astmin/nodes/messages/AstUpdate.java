package projects.astmin.nodes.messages;

import java.util.ArrayList;

import sinalgo.nodes.messages.Message;

public class AstUpdate extends Message {
    
    public ArrayList<Integer> peres;
    public ArrayList<Integer> nodes;
    public ArrayList<Integer> ranks;
    
    public AstUpdate() {
        this.peres = new ArrayList<Integer>();
        this.nodes = new ArrayList<Integer>();
        this.ranks = new ArrayList<Integer>();
    }

    @Override
    public Message clone() {
        AstUpdate a = new AstUpdate();
        a.peres = (ArrayList<Integer>) peres.clone();
        a.nodes = (ArrayList<Integer>) nodes.clone();
        a.ranks = (ArrayList<Integer>) ranks.clone();
        return a;
    }

}
